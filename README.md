# KeepStudying_Documentation

This project contains the Architecture and Design for the project

## Access the Documentation in Figma:
#### All the Documentation contents are included in individual pages in  Figma.
- User stories: https://www.figma.com/file/vCOLQARMvD7Rw9LWrRToPT/Keep-Studying?node-id=46%3A432
- Architectures: https://www.figma.com/file/vCOLQARMvD7Rw9LWrRToPT/Keep-Studying?node-id=1505%3A1023
- Wireframes: https://www.figma.com/file/vCOLQARMvD7Rw9LWrRToPT/Keep-Studying?node-id=2%3A2
- Prototype: https://www.figma.com/file/vCOLQARMvD7Rw9LWrRToPT/Keep-Studying?node-id=2%3A5

